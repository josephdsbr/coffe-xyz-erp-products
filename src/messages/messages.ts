export class Messages {
  // Validation
  static readonly DATABASE_PERSISTENCE_ERROR = 'Database persistence error.';
  static readonly FORBIDDEN_ACCESS = 'Forbidden access';
  static readonly ENTITY_NOT_FOUND = 'Entity not found';
  static readonly ENTITY_ALREADY_EXISTS = 'Entity already exists.';
  static readonly ENTITY_REMOVED_SUCCESSFULLY = 'Entity removed successfully.';
  static readonly PRODUCT_PRICE_CANNOT_BE_NEGATIVE_OR_ZERO =
    'Product price cannot be negative or zero.';
  static readonly PRODUCT_PRICE_CANNOT_BE_EMPTY =
    'Product prize cannot be empty.';
  static readonly PRODUCT_NAME_MIN_LENGTH_RESTRICTION =
    'Product name min length restriction';
  static readonly PRODUCT_NAME_MAX_LENGTH_RESTRICTION =
    'Product name max length restriction';
  static readonly PRODUCT_NAME_CANNOT_BE_EMPTY =
    'Product name cannot be empty.';
  static readonly PROVIDER_ERROR = 'Provider Error';
  static readonly USER_EMAIL_MUST_NOT_BE_EMPTY =
    'User email must not be empty.';
  static readonly USER_EMAIL_MUST_BE_VALID = 'User email must be valid.';
  static readonly USER_EMAIL_MAX_LENGTH_EXCEEDED =
    'User email max length exceeded';
  static readonly USER_NAME_MAX_LENGTH_EXCEEDED =
    'User name max length exceeded';
  static readonly USER_NAME_MUST_NOT_BE_EMPTY = 'User name must not be empty.';
  static readonly USER_PASSWORD_MUST_NOT_BE_NULL =
    'User password must no be null.';
  static readonly USER_PASSWORD_MIN_LENGTH_RESTRICTION =
    'User password min length restriction';
  static readonly USER_PASSWORD_CONFIRMATION_MUST_NOT_BE_NULL =
    'User password confirmation must no be null.';
  static readonly USER_PASSWORD_CONFIRMATION_MIN_LENGTH_RESTRICTION =
    'User password confirmation min length restriction';
  static readonly USER_EMAIL_DOES_NOT_EXIST = 'User email does not exist.';
  static readonly USER_PASSWORD_DOES_NOT_MATCH =
    'User password does not match.';
  static readonly USER_NOT_FOUND = 'User not found.';
  static readonly USER_NAME_MUST_BE_STRING = 'User name must be a string.';
  static readonly CARD_NUMBER_CANNOT_BE_EMPTY = 'Card Number cannot be empty.';
  static readonly CARD_NUMBER_MIN_LENGTH_RESTRICTION =
    'Card Number min length restriction';
  static readonly CARD_NUMBER_MAX_LENGTH_RESTRICTION =
    'Card Number max length restriction';
  static readonly CARD_VALIDATION_DATE_CANNOT_BE_EMPTY =
    'Card Validation Date cannot be empty.';
  static readonly CARD_VALIDATION_DATE_MUST_MATCH_PATTERN =
    'Card Validation Date must match pattern';
  static readonly CARD_CODE_CANNOT_BE_EMPTY = 'Card Code cannot be empty.';
  static readonly CARD_CODE_MIN_LENGTH_RESTRICTION =
    'Card Code min length restriction';
  static readonly CARD_CODE_MAX_LENGTH_RESTRICTION =
    'Card Code max length restriction';
  static readonly PAYMENT_AMOUNT_CANNOT_BE_EMPTY =
    'Payment Amount cannot be empty';
  static readonly PAYMENT_AMOUNT_MUST_BE_POSITIVE =
    'Payment Amount must be positive';
  static readonly CARD_OBJECT_CANNOT_BE_EMPTY_OR_HAVE_EMPTY_PROPERTIES =
    'Card object cannot be empty or have empty properties.';
  static readonly ITEM_PRODUCT_UID_CANNOT_BE_NULL =
    'Item Product UID cannot be null.';
  static readonly ITEM_AMOUNT_CANNOT_BE_NULL = 'Item Amount cannot be null.';
  static readonly ITEM_AMOUNT_MUST_BE_POSITIVE =
    'Item Amount must be positive.';
  static readonly PURCHASE_ITEMS_CANNOT_BE_EMPTY =
    'Purchase items cannot be empty.';
  static readonly PURCHASE_ITEMS_MIN_SIZE_RESTRICTION =
    'Purchase items min size restriction';
  static readonly PURCHASE_AMOUNT_MUST_BE_POSITIVE =
    'Purchase amount must be positive.';
  static readonly PURCHASE_AMOUNT_CANNOT_BE_EMPTY =
    'Purchase amount cannot be empty.';
  static ITEM_PRODUCT_DOES_NOT_EXIST = 'Item product does not exist';
}
