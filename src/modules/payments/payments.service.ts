import { BraspagGateway } from './providers/gateway/implementations/braspag.gateway';
import { HandlePaymentDTO } from './dtos/handle-payment.dto';
import { PaymentsRepository } from './payments.repository';
import { Injectable } from '@nestjs/common';
import { Payments } from './payments.entity';

@Injectable()
export class PaymentsService {
  constructor(
    private repository: PaymentsRepository,
    private gateway: BraspagGateway,
  ) {}

  async handlePayment(dto: HandlePaymentDTO): Promise<Payments> {
    const payment = await this.gateway.handlePayment(dto);
    return await this.repository.save(payment);
  }
}
