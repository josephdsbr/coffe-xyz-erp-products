import { Payments } from './payments.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Payments)
export class PaymentsRepository extends Repository<Payments> {}
