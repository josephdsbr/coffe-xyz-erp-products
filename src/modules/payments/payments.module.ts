import { BraspagGateway } from './providers/gateway/implementations/braspag.gateway';
import { PaymentsRepository } from './payments.repository';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PaymentsService } from './payments.service';

@Module({
  imports: [TypeOrmModule.forFeature([PaymentsRepository])],
  providers: [PaymentsService, BraspagGateway],
})
export class PaymentsModule {}
