import { DomainBaseEntity } from 'src/entity/domain-base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Payments extends DomainBaseEntity<Payments> {
  @Column()
  amount: number;
  @Column({ update: false })
  cardToken: string;

  setUpdatableFields(entity: Payments): void {
    this.amount = entity.amount;
  }
}
