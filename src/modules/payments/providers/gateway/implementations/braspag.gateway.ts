import { GatewayInterface } from './../gateway.interface';
import { Payments } from 'src/modules/payments/payments.entity';
import { HandlePaymentDTO } from 'src/modules/payments/dtos/handle-payment.dto';
import * as bcrypt from 'bcrypt';
import { Injectable } from '@nestjs/common';

@Injectable()
export class BraspagGateway implements GatewayInterface<HandlePaymentDTO> {
  /**
   * Simulando uma chamada ao Gateway da Braspag
   * @param payload
   */
  async handlePayment(payload: HandlePaymentDTO): Promise<Payments> {
    const payment = new Payments();
    payment.amount = payload.amount;
    const salt = await bcrypt.genSalt();
    payment.cardToken = bcrypt.hash(payload.card, salt);
    return Promise.resolve(payment);
  }
}
