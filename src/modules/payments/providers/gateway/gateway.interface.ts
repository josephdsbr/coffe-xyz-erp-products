import { Payments } from '../../payments.entity';

export interface GatewayInterface<T> {
  handlePayment(payload: T): Promise<Payments>;
}
