import { Messages } from 'src/messages/messages';
/* eslint-disable prettier/prettier */
import { IsNotEmpty, Matches, MaxLength, MinLength } from 'class-validator';
export class CardDTO {
  @IsNotEmpty({ message: Messages.CARD_NUMBER_CANNOT_BE_EMPTY })
  @MinLength(15, { message: `${Messages.CARD_NUMBER_MIN_LENGTH_RESTRICTION}: 15` })
  @MaxLength(17, { message: `${Messages.CARD_NUMBER_MAX_LENGTH_RESTRICTION}: 17` })
  number: string;

  @IsNotEmpty({ message: Messages.CARD_VALIDATION_DATE_CANNOT_BE_EMPTY })
  @Matches('/[\d]{2}\/[\d]{4}/', {} as any, { message: `${Messages.CARD_VALIDATION_DATE_MUST_MATCH_PATTERN}: mm/YYYY` })
  validationDate: string;

  @IsNotEmpty({ message: Messages.CARD_CODE_CANNOT_BE_EMPTY })
  @MinLength(3, { message: `${Messages.CARD_CODE_MIN_LENGTH_RESTRICTION}: 3` })
  @MaxLength(5, { message: `${Messages.CARD_CODE_MAX_LENGTH_RESTRICTION}: 5` })
  code: string;
  name: string;
}