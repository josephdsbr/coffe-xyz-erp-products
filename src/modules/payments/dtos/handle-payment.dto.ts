import { Messages } from 'src/messages/messages';
import {
  IsPositive,
  ValidateNested,
  IsNotEmptyObject,
  IsNotEmpty,
} from 'class-validator';
import { CardDTO } from './card.dto';
export class HandlePaymentDTO {
  @IsNotEmptyObject(
    {
      nullable: false,
    },
    { message: Messages.CARD_OBJECT_CANNOT_BE_EMPTY_OR_HAVE_EMPTY_PROPERTIES },
  )
  @ValidateNested()
  card: CardDTO;

  @IsNotEmpty({ message: Messages.PAYMENT_AMOUNT_CANNOT_BE_EMPTY })
  @IsPositive({ message: Messages.PAYMENT_AMOUNT_MUST_BE_POSITIVE })
  amount: number;
}
