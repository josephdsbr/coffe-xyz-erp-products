import { AwsStaticService } from './../statics/implementations/aws-static/aws-static.service';
import { PurchasesRepository } from './repository/purchases.repository';
import { ItemsRepository } from './repository/items.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { OrdersService } from './orders.service';
import { OrdersController } from './orders.controller';
import { ItemsService } from './services/items.service';
import { PurchasesService } from './services/purchases.service';
import { ProductsService } from '../products/products.service';
import { ProductsRepository } from '../products/products.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ItemsRepository,
      PurchasesRepository,
      ProductsRepository,
    ]),
  ],
  providers: [OrdersService, ItemsService, PurchasesService],
  controllers: [OrdersController],
})
export class OrdersModule {}
