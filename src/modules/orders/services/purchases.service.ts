import { Injectable } from '@nestjs/common';
import { PurchasesRepository } from '../repository/purchases.repository';

@Injectable()
export class PurchasesService {
  constructor(private purchasesRepository: PurchasesRepository) {}
}
