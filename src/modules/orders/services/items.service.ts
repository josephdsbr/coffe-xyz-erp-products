import { ProductsRepository } from './../../products/products.repository';
import { ItemsRepository } from './../repository/items.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class ItemsService {
  constructor(
    private itemsRepository: ItemsRepository,
    private productsRepository: ProductsRepository,
  ) {}

  async exists(uid: string): Promise<boolean> {
    const item = await this.productsRepository.findByUidAndRemovedAtIsNull(uid);
    return !!item;
  }
}
