import { Items } from '../items.entity';
import { ItemsDTO } from './../dtos/items.dto';
export class ItemsBuilder {
  static toEntity(dto: ItemsDTO): Items {
    return new Items(dto.amount);
  }
}
