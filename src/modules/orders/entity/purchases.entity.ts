import { Items } from './items.entity';
import { Payments } from './../../payments/payments.entity';
import { DomainBaseEntity } from 'src/entity/domain-base.entity';
import { Entity, JoinColumn, OneToMany, OneToOne } from 'typeorm';

@Entity()
export class Purchases extends DomainBaseEntity<Purchases> {
  @OneToOne(() => Payments)
  @JoinColumn()
  payments: Payments;

  @OneToMany(() => Items, (items: Items) => items.purchases)
  items: Items[];

  setUpdatableFields(entity: Purchases): void {
    Object.assign(this, entity);
  }
}
