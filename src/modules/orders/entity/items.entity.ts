import { Products } from './../../products/products.entity';
import { Purchases } from './purchases.entity';
import { DomainBaseEntity } from 'src/entity/domain-base.entity';
import { Column, Entity, ManyToOne, OneToOne } from 'typeorm';

@Entity()
export class Items extends DomainBaseEntity<Items> {
  @ManyToOne(() => Purchases, (purchases: Purchases) => purchases.items)
  purchases: Purchases;
  @OneToOne(() => Products)
  products: Products;
  @Column()
  amount: number;

  constructor(amount: number) {
    super();
    this.amount = amount;
  }

  setUpdatableFields(entity: Items): void {
    Object.assign(this, entity);
  }
}
