import { EntityRepository, Repository } from 'typeorm';
import { Items } from '../entity/items.entity';

@EntityRepository(Items)
export class ItemsRepository extends Repository<Items> {
  async findByUidAndRemovedAtIsNull(id: number): Promise<Items> {
    return this.findOne(id, { where: { removedAt: null } });
  }
}
