import { Messages } from 'src/messages/messages';
import { ProductsService } from './../products/products.service';
import { CreatePurchaseDTO } from './entity/dtos/create-purchase.dto';
import { PurchasesService } from './services/purchases.service';
import { ItemsService } from './services/items.service';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OrdersService {
  constructor(
    private itemsService: ItemsService,
    private purchasesService: PurchasesService,
  ) {}

  async create(dto: CreatePurchaseDTO): Promise<void> {}
}
