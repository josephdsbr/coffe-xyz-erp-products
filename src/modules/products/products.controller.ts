import { CreateProductDTO } from './dtos/create-product.dto';
import { ProductsService } from './products.service';
import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  UploadedFile,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { Products } from './products.entity';
import { Messages } from 'src/messages/messages';
import { FileInterceptor } from '@nestjs/platform-express';
import { CreateProductDTOBuilder } from './builders/create-product.dto.builder';
import { GrpcMethod } from '@nestjs/microservices';

interface UUID {
  data: string;
}

@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @Get()
  async getAll(): Promise<Products[]> {
    return await this.productsService.findAll();
  }

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  @UseInterceptors(ClassSerializerInterceptor)
  async create(
    @Body(ValidationPipe) createProductDTO: CreateProductDTO,
    @UploadedFile() file,
  ): Promise<void> {
    createProductDTO.price = Number(createProductDTO.price);
    await this.productsService.save(
      CreateProductDTOBuilder.toEntity(createProductDTO),
      file,
    );
  }

  @Delete(':uid')
  async remove(@Param('uid') uid: string): Promise<{ message: string }> {
    await this.productsService.remove(uid);
    return { message: `${Messages.ENTITY_REMOVED_SUCCESSFULLY}` };
  }

  @GrpcMethod('ProductsController', 'GetByUid')
  async getByUid(uid: UUID): Promise<Products> {
    return (await this.productsService.findByUid(uid.data)) || new Products();
  }

  @GrpcMethod('ProductsController', 'ProductExists')
  async productExists(uuid: UUID): Promise<{ exists: boolean }> {
    const exists = await this.productsService.exists(uuid.data);
    return { exists };
  }
}
