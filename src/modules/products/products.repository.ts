import { Products } from './products.entity';
import { EntityRepository } from 'typeorm';
import { BaseCrudRepository } from 'src/repository/base-crud.repository';

@EntityRepository(Products)
export class ProductsRepository extends BaseCrudRepository<Products> {}
