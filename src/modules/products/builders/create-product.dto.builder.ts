import { Products } from './../products.entity';
import { CreateProductDTO } from './../dtos/create-product.dto';
export class CreateProductDTOBuilder {
  static toEntity(dto: CreateProductDTO) {
    const { name, price } = dto;
    return new Products(name, price);
  }
}
