import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { Messages } from 'src/messages/messages';

export class CreateProductDTO {
  @IsNotEmpty({ message: Messages.PRODUCT_NAME_CANNOT_BE_EMPTY })
  @MaxLength(200, {
    message: `${Messages.PRODUCT_NAME_MAX_LENGTH_RESTRICTION}: 200`,
  })
  @MinLength(10, {
    message: `${Messages.PRODUCT_NAME_MIN_LENGTH_RESTRICTION}: 10`,
  })
  name: string;

  @IsNotEmpty({ message: Messages.PRODUCT_PRICE_CANNOT_BE_EMPTY })
  price: number;
}
