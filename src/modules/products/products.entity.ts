import { DomainBaseEntity } from 'src/entity/domain-base.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class Products extends DomainBaseEntity<Products> {
  @Column({ nullable: false })
  imageUrl: string;
  @Column({ nullable: false })
  name: string;
  @Column({ nullable: false })
  price: number;

  constructor(name: string = null, price: number = null) {
    super();
    this.name = name;
    this.price = price;
    this.imageUrl = null;
  }

  setUpdatableFields(entity: Products): void {
    this.imageUrl = entity.imageUrl;
    this.name = entity.name;
    this.price = entity.price;
    this.removedAt = entity.removedAt;
  }
}
