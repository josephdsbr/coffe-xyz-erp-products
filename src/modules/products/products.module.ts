import { AwsStaticService } from './../statics/implementations/aws-static/aws-static.service';
import { ProductsRepository } from './products.repository';
import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ProductsRepository])],
  providers: [ProductsService, AwsStaticService],
  controllers: [ProductsController],
})
export class ProductsModule {}
