import { AwsStaticService } from './../statics/implementations/aws-static/aws-static.service';
import { Messages } from '../../messages/messages';
import { ProductsRepository } from './products.repository';
import { Products } from './products.entity';
import { ForbiddenException, Injectable } from '@nestjs/common';

@Injectable()
export class ProductsService {
  constructor(
    private repository: ProductsRepository,
    private awsStaticService: AwsStaticService,
  ) {}

  protected getEntityName(): string {
    return Products.name;
  }

  protected async findEntityByProperty(entity: Products): Promise<Products> {
    return await this.repository.findByUidAndRemovedAtIsNull(entity.uuid);
  }

  async exists(uid: string): Promise<boolean> {
    const item = await this.repository.findByUidAndRemovedAtIsNull(uid);
    return !!item;
  }

  /**
   * TODO: Abstrair os métodos findAll, findByUid, save, update e remove para classe abstrata.
   */

  async findAll() {
    return await this.repository.findAllByRemovedAtIsNull();
  }

  async findByUid(uid: string) {
    return await this.repository.findByUidAndRemovedAtIsNull(uid);
  }

  async save(entity: Products, file: any) {
    const persistedEntity = await this.findEntityByProperty(entity);

    if (persistedEntity) {
      throw new ForbiddenException(
        `${Messages.ENTITY_ALREADY_EXISTS}: ${this.getEntityName()}`,
      );
    }

    const imageUrl = await this.awsStaticService.upload(file);
    entity.imageUrl = imageUrl;

    return this.repository.save(entity);
  }

  async update(entity: Products) {
    const persistedEntity = await this.findEntityByProperty(entity);

    if (!persistedEntity) {
      throw new ForbiddenException(
        `${Messages.ENTITY_NOT_FOUND}: ${this.getEntityName()}`,
      );
    }
    persistedEntity.setUpdatableFields(entity);
    return this.repository.save(entity);
  }

  async remove(uid: string) {
    const persistedEntity = await this.repository.findByUidAndRemovedAtIsNull(
      uid,
    );

    if (!persistedEntity) {
      throw new ForbiddenException(
        `${Messages.ENTITY_NOT_FOUND}: ${this.getEntityName()}`,
      );
    }
    persistedEntity.removedAt = new Date();
    return persistedEntity.save();
  }
}
