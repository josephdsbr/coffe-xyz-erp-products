import { StaticInterface } from './../static.interface';
import { Injectable } from '@nestjs/common';
import * as AWS from 'aws-sdk';

@Injectable()
export class AwsStaticService implements StaticInterface {
  AWS_S3_BUCKET_NAME = 'coffexyzbucket';

  async upload(file: any): Promise<string> {
    const urlKey = `filepath/${file.name ?? file.originalname}`;
    const params = {
      Body: file.buffer,
      Bucket: this.AWS_S3_BUCKET_NAME,
      Key: urlKey,
    };

    const data = await this.provider.upload(params).promise();

    return data.Location;
  }

  provider = new AWS.S3({
    accessKeyId: 'AKIAUIXOPPR3IID4J44P',
    secretAccessKey: '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',
  });
}
