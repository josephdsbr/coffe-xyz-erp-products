export interface StaticInterface {
  upload(file: any): string | Promise<string>;
}
