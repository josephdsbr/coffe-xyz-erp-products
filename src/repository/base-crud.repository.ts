import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { DomainBaseEntity } from '../entity/domain-base.entity';

@Injectable()
export class BaseCrudRepository<
  T extends DomainBaseEntity<T>
> extends Repository<T> {
  async findAllByRemovedAtIsNull(): Promise<T[]> {
    return await this.find({ where: { removedAt: null } });
  }

  async findByUidAndRemovedAtIsNull(uuid: string): Promise<T> {
    try {
      return await this.findOne({ where: { uuid: uuid, removedAt: null } });
    } catch (e) {
      return null;
    }
  }
}
